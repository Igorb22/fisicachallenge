package com.example.login;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.login.jogo.Opcao;

import java.io.IOException;
import java.util.List;

public class AdapterOpcao extends RecyclerView.Adapter<AdapterOpcao.OpcaoViewHolder> {
    private AdapterOpcao.OnItemClickListener mListener;
    private List<Opcao> opcoes;
    private Context ctx;

    public AdapterOpcao(List<Opcao> opcoes,Context context) {
        this.opcoes = opcoes;
        this.ctx = context;
    }

    @NonNull
    @Override
    public AdapterOpcao.OpcaoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_opcao, viewGroup, false);

        AdapterOpcao.OpcaoViewHolder holder = new AdapterOpcao.OpcaoViewHolder(view,mListener);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull OpcaoViewHolder opcaoViewHolder, int i) {
        opcaoViewHolder.txt.setText(opcoes.get(i).getOpcao());
    }


    public class OpcaoViewHolder extends RecyclerView.ViewHolder {
        final TextView txt;
        int position;

        public OpcaoViewHolder(View itemView, AdapterOpcao.OnItemClickListener listener) {
            super(itemView);
            this.txt = itemView.findViewById(R.id.txtOpcao);


            itemView.setOnClickListener(v -> {
                if (listener != null){
                    position = getAdapterPosition();
                    if(position != RecyclerView.NO_POSITION){
                        try {
                            listener.onItemClicked(position);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }

    }


    public void setOnItemClickListener(AdapterOpcao.OnItemClickListener listener){
        mListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClicked(int position) throws IOException;
    }

    @Override
    public int getItemCount() {
        return opcoes.size();
    }
}
