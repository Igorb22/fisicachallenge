package com.example.login;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;



/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {
    private Button btnEntrar;
    private ProgressDialog mProgressDialog;
    public static Usuario jogadorLogado;
    private Button btnCadastro;
    private CheckBox checkSenha;
    private boolean gravarSenha = false;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login, container, false);

        btnEntrar = v.findViewById(R.id.btnEntrar);
        //  pbEspera = v.findViewById(R.id.pbEspera);
        final EditText usuario = v.findViewById(R.id.usuario),
                senha = v.findViewById(R.id.senha);


        checkSenha = v.findViewById(R.id.checkSenha);


        checkSenha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(((CheckBox) v).isChecked())
                    gravarSenha = true;
                else
                    gravarSenha = false;


                Toast.makeText(getActivity().getBaseContext(), ""+gravarSenha, Toast.LENGTH_SHORT).show();
            }
        });



        // entra no app diretamente sem precisar pedir usuario e senha caso esteja gravado o login
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        if(sharedPreferences.contains("logado") && sharedPreferences.getBoolean("logado", false)) {
            try {
                jogadorLogado = getUsuarioLogado();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Toast.makeText(getActivity().getBaseContext(), "Objeto diferente de null", Toast.LENGTH_LONG).show();
            Intent j = new Intent(getActivity(), DashboardActivity.class);
            j.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(j);
            getActivity().finish();
        }


        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mProgressDialog = new ProgressDialog(getActivity());
                // Set progressdialog title
                mProgressDialog.setTitle("Login");
                // Set progressdialog message
                mProgressDialog.setMessage("Realizando Log In...");
                mProgressDialog.setIndeterminate(false);
                // Show progressdialog
                mProgressDialog.show();
                logIn(usuario.getText().toString(), senha.getText().toString());

            }
        });



        btnCadastro =  v.findViewById(R.id.btnCadastro);

        btnCadastro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.myFragment, new CadastroFragment());
                ft.commit();


                btnCadastro.setVisibility(View.INVISIBLE);
            }
        });

        return v;
    }

     public void logIn(final String user, final String pass) {
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        String url = "https://italabs.000webhostapp.com/FisicaChallenge/DownloadJogador.php";
        //requisicao para enviar via post para o servidor, que é capturado pelo arq php
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("Resposta", "" + response);

                mProgressDialog.dismiss();
                if (!response.equals("{}")) {
                    try {
                        //pegando o json do resultado da requisicao
                        JSONObject jogador = new JSONObject(response);

                        System.out.println("nome: " + jogador.getString("nome"));
                        if (jogador.getString("senha").equals(pass)) {
                            System.out.println("senha correta");

                            int pontuacao = 0, qtdPerguntas = 0;      //pontuacao e qtd perguntas
                            if (jogador.getString("pontuacao") != "null")
                                pontuacao = jogador.getInt("pontuacao");

                            if (jogador.getString("qtdPerguntas").intern() != "null")
                                qtdPerguntas = jogador.getInt("qtdPerguntas");

                            //----------------------------------------------------------------

                            // Toast.makeText(getActivity(), "Bem vindo " + jogador.getString("nome") + "!", Toast.LENGTH_LONG).show();
                            jogadorLogado = new Usuario(jogador.getInt("id"), jogador.getString("img"),
                                    jogador.getString("nome"), pontuacao, jogador.getString("usuario"),
                                    jogador.getString("senha"), qtdPerguntas);

                            // ---------------------------------------------------------------
                            // se usuario marcou em gravar senha, ele salva o login do usuario

                            sharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
                            editor = sharedPreferences.edit();
                            editor.putBoolean("logado", true);
                            editor.apply();

                         //   if (gravarSenha)
                                gravaUsuario();
                            // ---------------------------------------------------------------

                            Intent j = new Intent(getActivity(), DashboardActivity.class);
                            j.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(j);
                            getActivity().finish();

                        } else
                            Toast.makeText(getActivity(), "Senha incorreta, serás um impostor?", Toast.LENGTH_LONG).show();

                    } catch (JSONException ignore) {
                        Toast.makeText(getActivity(), "Problema indefinido, contacte os desenvolvedores.", Toast.LENGTH_LONG).show();
                        ignore.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else
                    Toast.makeText(getActivity(), "Usuário não encontrado!", Toast.LENGTH_LONG).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("O erro", "" + error);
                Toast.makeText(getActivity(), "" + error, Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("usuario", user);
                System.out.println(user);
                return param;
            }
        };
        requestQueue.add(stringRequest);
    }




    public Usuario getUsuarioLogado() throws IOException {
        File f = new File(getActivity().getFilesDir()+"/logado.txt");
        Usuario user = null;
        String linha = null;
        if (f.exists()) {
            BufferedReader br = new BufferedReader(new FileReader(f));

            linha = br.readLine();
            String [] dados = linha.split(";");

           user = new Usuario(Integer.parseInt(dados[0]), dados[4], dados[1],
                    Integer.parseInt(dados[5]), dados[2], dados[3], Integer.parseInt(dados[6]));

            br.close();
        }
        return user;
    }

    public void gravaUsuario() throws IOException {
        File f = new File(getActivity().getFilesDir(),"logado.txt");
        FileOutputStream fos = new FileOutputStream(f);

        fos.write((jogadorLogado.getCodigo() + ";"
                + jogadorLogado.getNome() + ";"
                + jogadorLogado.getUser() + ";"
                + jogadorLogado.getSenha() + ";"
                + jogadorLogado.getCaminhoImg() + ";"
                + jogadorLogado.getPontuacao() + ";"
                + jogadorLogado.getQtdPerguntas()).getBytes());

        Toast.makeText(getActivity().getBaseContext(),"Objeto gravado",Toast.LENGTH_LONG).show();

        fos.flush();
        fos.close();
    }

}
