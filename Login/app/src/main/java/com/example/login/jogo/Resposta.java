package com.example.login.jogo;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

public class Resposta extends RealmObject {
    private int codigoJogador;
    private int codigoPergunta;
    private int pontuacao;

    public Resposta() {}

    public void setCodigoJogador(int codigoJogador) {
        this.codigoJogador = codigoJogador;
    }

    public void setCodigoPergunta(int codigoPergunta) {
        this.codigoPergunta = codigoPergunta;
    }

    public void setPontuacao(int pontuacao) {
        this.pontuacao = pontuacao;
    }

    public int getCodigoJogador() {
        return codigoJogador;
    }

    public int getCodigoPergunta() {
        return codigoPergunta;
    }

    public int getPontuacao() {
        return pontuacao;
    }
}
