package com.example.login.jogo;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.example.login.AdapterOpcao;
import com.example.login.Comunicador;
import com.example.login.R;
import com.github.lzyzsd.circleprogress.DonutProgress;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

import static com.example.login.ConexaoBanco.perguntas;
import static com.example.login.LoginFragment.jogadorLogado;

/**
 * A simple {@link Fragment} subclass.
 */

public class PerguntaFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private AdapterOpcao adapterOpcao;
    private TextView txtPergunta;
    private CardView cardView;
    private TextView txtLog;
    private TextView txtPontos;
    private ImageView imgResposta;
    private Button btnContinuar;
    private Button btnFinalizar;
    // detalhe de interface
    private LinearLayout linearLayout;
    private int tempo;
    private int progressStatus;
    private DonutProgress donutProgress;
    private ProgressBar progressBar;
    private Comunicador comunicador;
    private int verificaProgress;
    ArrayList<Opcao> opcoes;

    public PerguntaFragment() {

    }

    @Override
    public void onActivityCreated(Bundle savedInstacedState){
        super.onActivityCreated(savedInstacedState);

        comunicador = (Comunicador) getActivity();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_perguntaragment, container, false);
        // -----------------------------------------------------------------------
        txtPergunta = v.findViewById(R.id.txtPergunta);
        // ----------------------------------------------------------------------
        if (perguntas.size() > 0) {
            // referencia dos itens do carView
            cardView = v.findViewById(R.id.myCard);
            txtPontos = v.findViewById(R.id.txtPontuacaoObtida);
            txtLog = v.findViewById(R.id.txtLogResposta);
            imgResposta = v.findViewById(R.id.imgResposta);
            btnContinuar = v.findViewById(R.id.btnContinuar);
            btnFinalizar = v.findViewById(R.id.btnFinalizar);
            // refenrencia dos itens do progressbar
            donutProgress = v.findViewById(R.id.donut_progress);
            // recycler
            mRecyclerView = v.findViewById(R.id.rcvOpcao);
            // pergunta
            txtPergunta = v.findViewById(R.id.txtPergunta);
            progressBar = v.findViewById(R.id.progress);
            linearLayout = v.findViewById(R.id.linear);

            // ------------------------------------------------------------------
            opcoes = new ArrayList<>();
            txtPergunta.setText("" + perguntas.get(0).getPergunta());

            Opcao op = new Opcao("" + perguntas.get(0).getAlternative1());
            opcoes.add(op);
            op = new Opcao("" + perguntas.get(0).getAlternative2());
            opcoes.add(op);
            op = new Opcao("" + perguntas.get(0).getAlternative3());
            opcoes.add(op);
            op = new Opcao("" + perguntas.get(0).getAlternative4());
            opcoes.add(op);
            // ---------------------------------------------------------------------
            //circle progress

            donutProgress.setFinishedStrokeColor(Color.parseColor("#512DA8"));
            donutProgress.setTextColor(Color.parseColor("#512DA8"));
            donutProgress.setKeepScreenOn(true);
            donutProgress.setSuffixText("s");
            //---------------------------------------------------------------------
            //Componentes do Recicler
            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), 1);
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setLayoutManager(layoutManager);

            adapterOpcao = new AdapterOpcao(opcoes, getContext());
            mRecyclerView.setAdapter(adapterOpcao);

            // -----------------------------------------------------------------------
            progressStatus =  perguntas.get(0).getTempo();
            int tmax  =perguntas.get(0).getTempo();
            donutProgress.setMax(tmax);
            // ProgressBar que conta o tempo de resposta do usuário
            Handler handler = new Handler();
            new Thread(new Runnable() {
                public void run() {
                    while (progressStatus >= 0) {
                        handler.post(new Runnable() {
                            public void run() {
                                donutProgress.setProgress(progressStatus);
                                verificaProgress = progressStatus;
                                if (progressStatus == 0) {
                                    menuOpcao(2);
                                }
                            }
                        });
                        try {
                            // Sleep for 1000 milliseconds.
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        progressStatus -= 1;
                    }
                }
            }).start();
            // -----------------------------------------------------------------------------
            adapterOpcao.setOnItemClickListener(position -> {
                tempo = progressStatus;
                progressStatus = 0;
                if (tempo > 0) {
                    // verificando se a opcao escolhida foi a correta
                    if (position == perguntas.get(0).getAlCorreta()-1) {
                        menuOpcao(0);
                        gravaResposta(); /* aqui o momento em que eu gravo a resposta,
                                          * quando o usuário clica, mas só grava se a resposta for correta */
                    }else
                        menuOpcao(1); /* resposta errada */
                }else
                    menuOpcao(2); /* tempo esgotado */
            });
            // -------------------------------------------------------------------------------
            btnContinuar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    linearLayout.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    // ------------------------------------------------------------------
                    // removendo o primeiro elemento do array, assim sempre podemos selecionar a pergunta de indice 0
                    perguntas.remove(0);
                    // reiniciando fragment
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.replace(R.id.containerFragment, new PerguntaFragment()).commit();

                }
            });



            btnFinalizar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // aqui a requisicão
                    linearLayout.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    // ------------------------------------------------------------------
                    comunicador.fimJogo(true);
                }
            });
            // -------------------------------------------------------------------------------
        }else {
            //donutProgress.setVisibility(View.GONE);
            txtPergunta.setText("As perguntas acabaram, tente novamente mais tarde!");
        }

        return v;
    }

    public void menuOpcao(int resposta){
        if (cardView.getVisibility() == View.GONE){
            if (resposta == 0){
                txtLog.setText("Resposta Correta!");
                txtPontos.setText("Pontuacao: "+tempo*10);
                imgResposta.setImageResource(R.drawable.certo);
            }else if (resposta == 1){
                txtLog.setText("Resposta Errada!");
                imgResposta.setImageResource(R.drawable.errado);
                txtPontos.setText("Pontuação: "+0);
            }else if (resposta == 2){
                txtLog.setText("Seu tempo Acabou!");
                imgResposta.setImageResource(R.drawable.errado);
                txtPontos.setText("Pontuação: "+0);
            }
            cardView.setVisibility(View.VISIBLE);
        }
    }

    public void gravaResposta() {
      //  int idJogador = 1; // id ficticio do usuario
        Resposta r = new Resposta();
        r.setCodigoJogador(jogadorLogado.getCodigo());
        r.setCodigoPergunta(perguntas.get(0).getCodigo());
        r.setPontuacao(tempo*10);
        //grava no arquiv a pontucao atualiza pra n precisar ir no banco e gravar e pedquidar novamente
        jogadorLogado.setPontuacao(jogadorLogado.getPontuacao()+r.getPontuacao());
        jogadorLogado.setQtdPerguntas(jogadorLogado.getQtdPerguntas()+1);
        try {
            gravaUsuario();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        realm.copyToRealm(r);

        RealmResults<Resposta> respostas = realm.where(Resposta.class).findAll();

        Toast.makeText(getActivity(),"jogador: "
                + respostas.get(0).getCodigoJogador()
                +" pergunta: "+respostas.get(0).getCodigoPergunta()
                +" pontuacao: " + respostas.get(0).getPontuacao(),Toast.LENGTH_LONG)
                .show();

        realm.commitTransaction();
        realm.close();
    }

    public void gravaUsuario() throws IOException {
        File f = new File(getActivity().getFilesDir(),"logado.txt");
        FileOutputStream fos = new FileOutputStream(f);

        fos.write((jogadorLogado.getCodigo() + ";"
                + jogadorLogado.getNome() + ";"
                + jogadorLogado.getUser() + ";"
                + jogadorLogado.getSenha() + ";"
                + jogadorLogado.getCaminhoImg() + ";"
                + jogadorLogado.getPontuacao() + ";"
                + jogadorLogado.getQtdPerguntas()).getBytes());

        Toast.makeText(getActivity().getBaseContext(),"Objeto gravado",Toast.LENGTH_LONG).show();

        fos.flush();
        fos.close();
    }
}

