package com.example.login;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class RankingFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private AdapterRanking adapterRanking;
    private ArrayList<Usuario> rankings;
    private ProgressBar pro;
    public RankingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_ranking, container, false);

        //usuarios  = new ArrayList<>();

        // Usuario u = new Usuario(1,"","IGOR BRUNO DOS SANTOS",256, 0);
        // usuarios.add(u);
        /// u = new Usuario(45,"","JOSIELY DE OLIVEIRA",338, 0);
        //  usuarios.add(u);
        //// u = new Usuario(52,"","ABRAÃO PEREIRA ALVES",412, 0);
        // usuarios.add(u);

        mRecyclerView = v.findViewById(R.id.rcvRanking);
        pro = v.findViewById(R.id.progressBar);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), 1);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(layoutManager);

        loadRanking();

        return v;
    }

    public void loadRanking(){
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        String url = "https://italabs.000webhostapp.com/FisicaChallenge/DownloadRank.php";
        //requisicao para enviar via post para o servidor, que é capturado pelo arq php
        rankings  = new ArrayList<>();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                url, null, response -> {
            if(response.toString().equals("[]")){
                System.out.println("Sem ranking");
            }
            else{
                try {
                    // Toast.makeText(context,"TO DENTRO DO METODO PORRA",Toast.LENGTH_LONG).show();
                    JSONArray ranking = response.getJSONArray("rank");
                    for (int i = 0; i < ranking.length(); i++) {
                        JSONObject rank = ranking.getJSONObject(i);
                        Usuario u = new Usuario(rank.getString("img"),rank.getString("nome"),
                                rank.getInt("pontuacao"), rank.getInt("qtd"));
                        rankings.add(u);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                pro.setVisibility(View.GONE);
                adapterRanking = new AdapterRanking(rankings,getContext());
                mRecyclerView.setAdapter(adapterRanking);
            }


        }, error -> Log.d("ranking", error.getMessage()));
        requestQueue.add(jsonObjectRequest);
    }

}
