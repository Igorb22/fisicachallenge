package com.example.login;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Usuario extends RealmObject {
    @PrimaryKey
    private int codigo;
    private String caminhoImg;
    private String nome;
    private int pontuacao;
    private String user;
    private String senha;
    private int qtdPerguntas;


    public Usuario() {}

    public Usuario(int codigo, String caminhoImg, String nome, int pontuacao, String user,
                   String senha, int nQuestoes) {
        this.codigo = codigo;
        this.caminhoImg = caminhoImg;
        this.nome = nome;
        this.pontuacao = pontuacao;
        this.user = user;
        this.senha = senha;
        this.qtdPerguntas = nQuestoes;
    }

    public Usuario(String caminho ,String nome, int pontuacao, int qtdPerguntas) {
        this.caminhoImg = caminho;
        this.nome = nome;
        this.pontuacao = pontuacao;
        this.qtdPerguntas = qtdPerguntas;
    }


    public Usuario(int  codigo,String caminho ,String nome, int pontuacao,int qtdPerguntas) {
        this.codigo = codigo;
        this.caminhoImg = caminho;
        this.nome = nome;
        this.pontuacao = pontuacao;
        this.qtdPerguntas = qtdPerguntas;

    }


    public int getQtdPerguntas() {
        return qtdPerguntas;
    }

    public void setQtdPerguntas(int qtdPerguntas) {
        this.qtdPerguntas = qtdPerguntas;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }


    public String getUser() {
        return user;
    }

    public String getSenha() {
        return senha;
    }


    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public void setCaminhoImg(String caminhoImg) {
        this.caminhoImg = caminhoImg;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setPontuacao(int pontuacao) {
        this.pontuacao = pontuacao;
    }

    public int getCodigo() {
        return codigo;
    }

    public String getCaminhoImg() {
        return caminhoImg;
    }

    public String getNome() {
        return nome;
    }

    public int getPontuacao() {
        return pontuacao;
    }
}
