package com.example.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.login.jogo.PerguntaFragment;

import static com.example.login.ConexaoBanco.perguntas;

public class DashboardActivity extends AppCompatActivity implements Comunicador,Runnable,View.OnClickListener{
    private ImageButton btnJogar;
    private TextView txtJogar;
    private boolean status = false;
    private PerguntaFragment perguntaragment = null;
    private PerfilFragment perfilFragment = null;
    private RankingFragment rankingFragment = null;
    private CardView cdView;
    private Button btnContinuarPartida;
    private Button btnEncerrarPartida;
    private boolean finalizouJogo = false;
    private int identificadorLayout = 0; // 0 para PerfilFragemnt, 1 para RankingFragment e 2 para PerguntaFragment

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:

                    identificadorLayout = 0;

                    btnJogar.setVisibility(View.GONE);
                    txtJogar.setVisibility(View.GONE);

                    if (perfilFragment == null){ /* verifca se o proprio componente ja foi
                     * instanciado, ou então ele da erro no layout */
                        if (perguntaragment == null) { // se o usuario não estiver jogando
                            if (rankingFragment != null)
                                rankingFragment = null;
                            instanciaFragment(identificadorLayout);
                        } else if ((perguntas.size() == 0) ) { // se o usuario finalizou o jogo
                            instanciaFragment(identificadorLayout);
                            perguntaragment = null;
                        } else if (finalizouJogo){
                            instanciaFragment(identificadorLayout);
                            perguntaragment = null;
                        } else  // se o usuario estiver no meio de uma partida
                            cdView.setVisibility(View.VISIBLE);
                    }

                    return true;
                case R.id.navigation_dashboard:

                    if (perfilFragment != null || rankingFragment != null) { /* verifca se algum fragment
                     * ta ativo atualmente na tela*/
                        removeFragment();
                        perfilFragment = null;
                        rankingFragment = null;
                    }

                    if (status==false){ // torna o botao JOGAR visivel caso o fragment perguntaFragment seja requisitado
                        btnJogar.setVisibility(View.VISIBLE);
                        txtJogar.setVisibility(View.VISIBLE);
                    }
                    return true;

                case R.id.navigation_notifications:
                    identificadorLayout = 1;

                    btnJogar.setVisibility(View.GONE);
                    txtJogar.setVisibility(View.GONE);

                    if (rankingFragment == null) {  /* verifca se o proprio componente ja foi
                     * instanciado, ou então ele da erro no layout */
                        if (perguntaragment == null) {  // se o usuario não estiver jogando
                            if (perfilFragment != null)
                                perfilFragment = null;
                            instanciaFragment(identificadorLayout);
                        } else if ((perguntas.size() == 0)) {  // se o usuario finalizou o jogo
                            instanciaFragment(identificadorLayout);
                            perguntaragment = null;
                        } else if (finalizouJogo) {
                            instanciaFragment(identificadorLayout);
                            perguntaragment  = null;
                        } else // se o usuario estiver no meio de uma partida
                            cdView.setVisibility(View.VISIBLE);
                    }
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        instanciaFragment(identificadorLayout);
        btnJogar = findViewById(R.id.btnJogar);
        txtJogar = findViewById(R.id.txtJogar);
        cdView  = findViewById(R.id.cardEncerrarPartida);
        btnContinuarPartida = findViewById(R.id.btnContinuarPartida);
        btnEncerrarPartida = findViewById(R.id.btnEncerrarPartida);


        btnContinuarPartida.setOnClickListener(this);
        btnEncerrarPartida.setOnClickListener(this);
        btnJogar.setOnClickListener(this);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }


    public void removeFragment(){
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.containerFragment);
        FragmentTransaction ft = fm.beginTransaction();
        ft.remove(fragment).commit();
    }


    public void instanciaFragment(int i){
        if (i == 0) {
            perfilFragment = new PerfilFragment();
            final FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.containerFragment, perfilFragment);
            ft.commit();
        } else if (i ==1){
            rankingFragment = new RankingFragment();
            final FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.containerFragment, rankingFragment);
            ft.commit();
        } else if (i == 2){
            perguntaragment = new PerguntaFragment();
            final FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.containerFragment, perguntaragment);
            ft.commit();
        }
        status = false;
        identificadorLayout = -1;
    }

    public void run() {
        if (!isConected(this)) {
            Toast.makeText(getBaseContext(), "Você não tem conexão com a internet", Toast.LENGTH_SHORT).show();
        }
    }

    private static boolean isConected(Context cont) {
        ConnectivityManager conmag = (ConnectivityManager) cont.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (conmag != null) {
            conmag.getActiveNetworkInfo();

            //Verifica internet pela WIFI
            if (conmag.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected()) {
                return true;
            }

            //Verifica se tem internet móvel
            return conmag.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnected();
        }
        return false;
    }

    @Override
    public void fimJogo(boolean status) {
        if (status) {
            removeFragment();
            this.status = false;
            btnJogar.setVisibility(View.VISIBLE);
            txtJogar.setVisibility(View.VISIBLE);
            this.finalizouJogo = status;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnContinuarPartida:
                cdView.setVisibility(View.GONE);
                break;
            case R.id.btnEncerrarPartida:
                cdView.setVisibility(View.GONE);
                perguntaragment = null;
                instanciaFragment(identificadorLayout);
                break;
            case R.id.btnJogar:
                btnJogar.setVisibility(View.INVISIBLE);
                txtJogar.setVisibility(View.INVISIBLE);
                status = true;
                identificadorLayout = 2;
                finalizouJogo = false;
                instanciaFragment(identificadorLayout);
                break;
        }
    }
}


