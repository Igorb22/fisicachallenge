package com.example.login;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;

public class AdapterRanking extends RecyclerView.Adapter<AdapterRanking.RankingViewHolder> {
    private AdapterRanking.OnItemClickListener mListener;
    private List<Usuario> usuarios;
    private Context context;

    public AdapterRanking( List<Usuario> usuarios, Context context) {
        this.usuarios = usuarios;
        this.context = context;
    }

    @NonNull
    @Override
    public AdapterRanking.RankingViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_ranking, viewGroup, false);
        AdapterRanking.RankingViewHolder holder = new AdapterRanking.RankingViewHolder(view,mListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RankingViewHolder  viewHolder, int i) {
            //viewHolder.img baixar a imagem e setar
            viewHolder.nome.setText(usuarios.get(i).getNome());
            viewHolder.pontuacao.setText(""+usuarios.get(i).getPontuacao());
            viewHolder.posicao.setText(""+(i+1));
     }

    public class RankingViewHolder extends RecyclerView.ViewHolder{
        ImageView img;
        TextView nome;
        TextView pontuacao;
        TextView posicao;
        int position;

        public RankingViewHolder(@NonNull View itemView, AdapterRanking.OnItemClickListener listener) {
            super(itemView);
            this.img = itemView.findViewById(R.id.imgPerfil);
            this.nome = itemView.findViewById(R.id.txtUsuario);
            this.pontuacao = itemView.findViewById(R.id.txtPontuacao);
            this.posicao = itemView.findViewById(R.id.txtPosicao);
            itemView.setOnClickListener(v -> {
                if (listener != null){
                    position = getAdapterPosition();
                    if(position != RecyclerView.NO_POSITION){
                        listener.onItemClicked(position);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return usuarios.size();
    }

    public void setOnItemClickListener(AdapterRanking.OnItemClickListener listener){
        mListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClicked(int position);
    }

}
