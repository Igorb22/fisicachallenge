package com.example.login;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.login.jogo.Pergunta;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Map;

public class ConexaoBanco {
    public static ArrayList<Pergunta> perguntas;
    public static String url = "https://italabs.000webhostapp.com/FisicaChallenge/AndroidDownload.php";
    private RequestQueue requestQueue;
    Map<String, String> params ;

    public ConexaoBanco() { this.perguntas = new ArrayList<>(); }

    public void setPerguntas(ArrayList<Pergunta> perguntas) {
        this.perguntas = perguntas;
    }

    public ArrayList<Pergunta> getPerguntas() {
        return perguntas;
    }

    public void loadPerguntas(Context context){
        requestQueue = Volley.newRequestQueue(context);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                url, null, response -> {
            try {
                Toast.makeText(context,"TO DENTRO DO METODO PORRA",Toast.LENGTH_LONG).show();
                JSONArray pergunta = response.getJSONArray("perguntas");
                for (int i = 0; i < pergunta.length(); i++) {
                    JSONObject per = pergunta.getJSONObject(i);
                    Pergunta p = new Pergunta();
                    p.setCodigo(per.getInt("codigo"));
                    p.setDificuldade(per.getString("dificuldade"));
                    p.setTempo(per.getInt("tempo"));
                    p.setPergunta(per.getString("pergunta"));
                    p.setAlternative1(per.getString("altenativa1"));
                    p.setAlternative2(per.getString("altenativa2"));
                    p.setAlternative3(per.getString("altenativa3"));
                    p.setAlternative4(per.getString("altenativa4"));
                    p.setAlCorreta(per.getInt("alternativaCorreta"));
                    //resultado
                    perguntas.add(p);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, error -> Log.d("pergunta", error.getMessage()));
        requestQueue.add(jsonObjectRequest);
    }
}
