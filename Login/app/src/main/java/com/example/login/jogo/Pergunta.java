package com.example.login.jogo;

public class Pergunta {
    private int codigo;
    private String dificuldade;
    private int tempo;
    private String pergunta;
    private String alternative1;
    private String alternative2;
    private String alternative3;
    private String alternative4;
    private int alCorreta;

    public Pergunta() {
        codigo = -1;
        dificuldade = null;
        tempo = 0;
        pergunta = null;
        alternative1 = null;
        alternative2 = null;
        alternative3 = null;
        alternative4 = null;
        alCorreta = 0;
    }

    public Pergunta(int codigo, String dificuldade, int tempo, String pergunta,
                    String alternative1, String alternative2, String alternative3, String alternative4, int alCorreta) {
        this.codigo = codigo;
        this.dificuldade = dificuldade;
        this.tempo = tempo;
        this.pergunta = pergunta;
        this.alternative1 = alternative1;
        this.alternative2 = alternative2;
        this.alternative3 = alternative3;
        this.alternative4 = alternative4;
        this.alCorreta = alCorreta;
    }


    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public void setDificuldade(String dificuldade) {
        this.dificuldade = dificuldade;
    }

    public void setTempo(int tempo) {
        this.tempo = tempo;
    }

    public void setPergunta(String pergunta) {
        this.pergunta = pergunta;
    }

    public void setAlternative1(String alternative1) {
        this.alternative1 = alternative1;
    }

    public void setAlternative2(String alternative2) {
        this.alternative2 = alternative2;
    }

    public void setAlternative3(String alternative3) {
        this.alternative3 = alternative3;
    }

    public void setAlternative4(String alternative4) {
        this.alternative4 = alternative4;
    }

    public void setAlCorreta(int alCorreta) {
        this.alCorreta = alCorreta;
    }

    public int getCodigo() {
        return codigo;
    }

    public String getDificuldade() {
        return dificuldade;
    }

    public int getTempo() {
        return this.tempo;
    }

    public String getPergunta() {
        return pergunta;
    }

    public String getAlternative1() {
        return alternative1;
    }

    public String getAlternative2() {
        return alternative2;
    }

    public String getAlternative3() {
        return alternative3;
    }

    public String getAlternative4() {
        return alternative4;
    }

    public int getAlCorreta() {
        return alCorreta;
    }
}
