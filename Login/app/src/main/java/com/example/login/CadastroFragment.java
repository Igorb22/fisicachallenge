package com.example.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class CadastroFragment extends Fragment {
    private Button btnCadastrar, btnEscolher;

    private static final int PICK_IMAGE_REQUEST= 99;
    private static final int PERMISSION_REQUEST_CODE = 1;
    private Bitmap bitmap;
    private ProgressDialog mProgressDialog;

    public CadastroFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_cadastro, container, false);

        if (!checkPermission()) {
            //se n tiver permissao
            requestPermission();
        }

        btnCadastrar = v.findViewById(R.id.btnCadastrar);
        btnEscolher = v.findViewById(R.id.escolher_foto);

        final EditText nome = v.findViewById(R.id.txtNome),
                usuario = v.findViewById(R.id.txtUser),
                senha = v.findViewById(R.id.senha),
                senha2 = v.findViewById(R.id.senha2);

        btnEscolher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });

        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!senha.getText().toString().equals(senha2.getText().toString()))
                    Toast.makeText(getActivity(), "Por favor, corrija as senhas, pois não estão iguais", Toast.LENGTH_LONG).show();
                else{
                        mProgressDialog = new ProgressDialog(getActivity());
                        // Set progressdialog title
                        mProgressDialog.setTitle("Cadastrando jogador");
                        // Set progressdialog message
                        mProgressDialog.setMessage("Carregando...");
                        mProgressDialog.setIndeterminate(false);
                        // Show progressdialog
                        mProgressDialog.show();
                    uploadJogador(nome.getText().toString(), usuario.getText().toString(), senha.getText().toString());
                }

            }
        });

        return v;
    }

    //intent de pegar a foto da memoria interna
    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                //pega a imagem da galeria
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);
                btnEscolher.setText("Foto selecionada");
                btnEscolher.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void uploadJogador(String nome, String usuario, String senha){

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        //requisicao para enviar via post para o servidor, que é capturado pelo arq php
        //ip remoto do arq php q vai pegar as informacoes

        String url = "https://italabs.000webhostapp.com/FisicaChallenge/UploadJogador.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, response -> {
            mProgressDialog.dismiss();
            Log.i("Resposta",""+response);
            Intent i = getActivity().getIntent();
            getActivity().finish();
            startActivity(i);

        }, error -> {
            Log.i("Erro",""+error);

        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String,String> param = new HashMap<>();

                String imagem = getStringImage(bitmap);
                Log.i("StringImage",""+imagem);

                param.put("nome",nome);
                param.put("usuario",usuario);
                param.put("senha",senha);
                param.put("img",imagem);
                return param;
            }
        };

        requestQueue.add(stringRequest);
    }

    //metodo para pegsr o bitmap da imagem capturada da intent
    private String getStringImage(Bitmap bitmap){

        Log.i("Bitmap",""+bitmap);
        ByteArrayOutputStream baos=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100, baos);
        byte [] b=baos.toByteArray();
        String temp= Base64.encodeToString(b, Base64.DEFAULT);

        return temp;
    }

    //ṕermissoes
    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(getActivity(), "Por favor, aceite as permissões do app", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity(), "Permissão aceita com sucesso", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), "Permissão negada", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

}
