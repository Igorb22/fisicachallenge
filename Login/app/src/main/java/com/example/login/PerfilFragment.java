package com.example.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import static com.example.login.LoginFragment.jogadorLogado;

/**
 * A simple {@link Fragment} subclass.
 */
public class PerfilFragment extends Fragment {

    private TextView pontuacao, nome, qtd;
    private ImageView perfil;
    private ImageButton btnCriarPergunta;
    private Button btnSair;
    public PerfilFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_perfil, container, false);

        btnCriarPergunta = v.findViewById(R.id.btnCriarPergunta);
        btnSair = v.findViewById(R.id.btnSair);

        perfil = v.findViewById(R.id.foto_perfil);
        nome = v.findViewById(R.id.nome_perfil);
        pontuacao = v.findViewById(R.id.pontuacao);
        qtd = v.findViewById(R.id.qtd_perguntas);

        nome.setText(jogadorLogado.getNome());
        pontuacao.setText(""+jogadorLogado.getPontuacao());
        qtd.setText(""+jogadorLogado.getQtdPerguntas());
        Glide.with(getActivity())
                .load(jogadorLogado.getCaminhoImg())
                .into(perfil);


        btnCriarPergunta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.containerFragment, new CadastrarPerguntaFragment());
                ft.commit();
            }
        });

        btnSair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logOut();
            }
        });

        return v;
    }

    private void logOut(){
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.putBoolean("logado", false);
        editor.apply();
        Intent i = new Intent(getActivity(), MainActivity.class);
        startActivity(i);
        getActivity().finish();
    }
}