package com.example.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */

public class CadastrarPerguntaFragment extends Fragment  {

    private Button btnCdadastrarPergunta;
    private EditText pergunta,alternativa1,alternativa2,alternativa3,alternativa4,tempo;
    private String dificuldade;
    private RadioGroup opcoes;
    private int resposta = 0;
    private ProgressDialog mProgressDialog;

    public CadastrarPerguntaFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_cadastrar_pergunta, container, false);

        pergunta = v.findViewById(R.id.pergunta);
        alternativa1 = v.findViewById(R.id.alternativa1);
        alternativa2 = v.findViewById(R.id.alternativa2);
        alternativa3 = v.findViewById(R.id.alternativa3);
        alternativa4 = v.findViewById(R.id.alternativa4);
        tempo = v.findViewById(R.id.tempo);

        btnCdadastrarPergunta = v.findViewById(R.id.btnCadastrarPergunta);
        Spinner spinner = (Spinner) v.findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.dif_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                dificuldade = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //dificuldade = "Fácil";
            }
        });

        opcoes = (RadioGroup) v.findViewById(R.id.opcoes);
        opcoes.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton resp = (RadioButton) group.findViewById(checkedId);
                resposta = Integer.parseInt(resp.getTag().toString());
                // Toast.makeText(getContext(), " resp " + resposta, Toast.LENGTH_LONG).show();
            }
        });

        btnCdadastrarPergunta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(resposta == 0)
                    Toast.makeText(getContext(), " resp " + resposta, Toast.LENGTH_LONG).show();
                else
                {
                    mProgressDialog = new ProgressDialog(getActivity());
                    // Set progressdialog title
                    mProgressDialog.setTitle("Cadastrando Pergunta");
                    // Set progressdialog message
                    mProgressDialog.setMessage("Carregando...");
                    mProgressDialog.setIndeterminate(false);
                    // Show progressdialog
                    mProgressDialog.show();

                    perguntaUpload();

                }
                // Toast.makeText(getContext(), " Acao " + resposta, Toast.LENGTH_LONG).show();
            }
        });

        return v;
    }

    public void perguntaUpload(){
        String url = "https://italabs.000webhostapp.com/FisicaChallenge/UploadPergunta.php";
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        //requisicao para enviar via post para o servidor, que é capturado pelo arq php
        //ip remoto do arq php q vai pegar as informacoes

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, response -> {
            mProgressDialog.dismiss();
            Log.i("Resposta",""+response);
            final FragmentManager fm = getActivity().getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.containerFragment, new PerfilFragment());
            ft.commit();

        }, error -> {
            Log.i("Erro",""+error);

        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String,String> param = new HashMap<>();
                System.out.println(" " + pergunta.getText().toString() + " "+ alternativa1.getText().toString()
                + " " + resposta + " " + dificuldade);
                param.put("pergunta",pergunta.getText().toString());
                param.put("alt1", alternativa1.getText().toString());
                param.put("alt2", alternativa2.getText().toString());
                param.put("alt3", alternativa3.getText().toString());
                param.put("alt4", alternativa4.getText().toString());
                param.put("tempo", tempo.getText().toString());
                param.put("dif", dificuldade);
                param.put("correta", ""+resposta);

                return param;
            }
        };

        requestQueue.add(stringRequest);


    }
}
